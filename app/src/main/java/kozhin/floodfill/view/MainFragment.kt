package kozhin.floodfill.view

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.jakewharton.rxbinding2.widget.RxSeekBar
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main.*
import kozhin.floodfill.App
import kozhin.floodfill.R
import kozhin.floodfill.model.AlgorithmResult
import kozhin.floodfill.model.Position
import kozhin.floodfill.presenter.MainPresenter
import javax.inject.Inject

class MainFragment : Fragment(), MainView {

    companion object {
        const val TAG = "MainFragment"
    }

    private lateinit var image1: ImageView
    private lateinit var image2: ImageView

    @Inject
    lateinit var presenter: MainPresenter

    private val size = Point(64, 64)

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onAttach(context: Context) {
        App.applicationComponent?.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.bindView(this)

        image1 = container1.findViewById(R.id.image)
        image2 = container2.findViewById(R.id.image)

        RxView.touches(image1)
                .filter { image1.drawable != null }
                .filter { it.action == MotionEvent.ACTION_DOWN }
                .observeOn(Schedulers.computation())
                .flatMap {
                    startFloodFilling(image2, it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            when (it.position) {
                                Position.FIRST -> showFirstResult(it.bitmap)
                                Position.SECOND -> showSecondResult(it.bitmap)
                            }
                        },
                        { showMessage(R.string.error_algorithm) }
                )

        RxView.touches(image2)
                .filter { image2.drawable != null }
                .filter { it.action == MotionEvent.ACTION_DOWN }
                .observeOn(Schedulers.computation())
                .flatMap {
                    startFloodFilling(image2, it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            when (it.position) {
                                Position.FIRST -> showFirstResult(it.bitmap)
                                Position.SECOND -> showSecondResult(it.bitmap)
                            }
                        },
                        { showMessage(R.string.error_algorithm) }
                )

        RxView.clicks(generate_button)
                .doOnNext {
                    image1.setImageDrawable(null)
                    image2.setImageDrawable(null)
                    showProgress()
                }
                .map { presenter.generateBitmap(size.x, size.y) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    hideProgress()
                    showResult(it)
                }


        createBitmapSizeClickObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()


        createSpinnerItemsListeners()
        RxSeekBar.userChanges(speed_seekbar).subscribe { presenter.setFillSpeed(it) }

    }

    private fun startFloodFilling(view: ImageView, event: MotionEvent) : Observable<AlgorithmResult>? {
        val bitmap = getBitmap(view)
        bitmap?.let {
            val viewCoords = IntArray(2)
            view.getLocationOnScreen(viewCoords)
            val maxImgX = view.width
            val maxImgY = view.height
            val absX = event.rawX
            val absY = event.rawY
            return presenter.startFloodFilling(viewCoords, maxImgX, maxImgY, bitmap, absX, absY)
        }
        return null
    }


    private fun getBitmap(view: ImageView): Bitmap? {
        return (view.drawable as? BitmapDrawable)?.bitmap
    }

    private fun createBitmapSizeClickObservable(): Completable = Completable.create { emitter ->
        val dialogView = layoutInflater.inflate(R.layout.dialog_size, null)

        val dialogBuilder = AlertDialog.Builder(context!!)
        val dialog = dialogBuilder.create()
        dialog.setView(dialogView)

        val cancelButton = dialogView.findViewById<Button>(R.id.cancel_button)
        val okButton = dialogView.findViewById<Button>(R.id.ok_button)

        val width = dialogView.findViewById<EditText>(R.id.edit_width)
        val height = dialogView.findViewById<EditText>(R.id.edit_height)

        okButton.isEnabled = false

        val widthWatcherObservable = RxTextView.textChanges(width)
        val heightWatcherObservable = RxTextView.textChanges(width)

        Observable.merge(widthWatcherObservable, heightWatcherObservable)
                .subscribe {
                    okButton.isEnabled = width.text.isNotEmpty() && height.text.isNotEmpty()
                }

        RxView.clicks(size_button).subscribe { dialog.show() }

        RxView.clicks(okButton).subscribe {
            size.x = width.text.toString().toInt()
            size.y = height.text.toString().toInt()
            width.hint = width.text
            height.hint = height.text
            dialog.dismiss()
            emitter.onComplete()
        }

        RxView.clicks(cancelButton).subscribe { dialog.dismiss() }
    }

    private fun createSpinnerItemsListeners() {
        val adapter = ArrayAdapter.createFromResource(context,
                R.array.algorithms_array, R.layout.spinner_item)

        val spinner1 = container1.findViewById<Spinner>(R.id.spinner)
        val spinner2 = container2.findViewById<Spinner>(R.id.spinner)

        spinner1.adapter = adapter
        spinner2.adapter = adapter

        RxAdapterView.itemSelections(spinner1)
                .subscribe { presenter.selectFirstAlgorithm(it) }

        RxAdapterView.itemSelections(spinner2)
                .subscribe { presenter.selectSecondAlgorithm(it) }
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showResult(bitmap: Bitmap) {
        image1.setImageBitmap(bitmap)
        image2.setImageBitmap(bitmap)
    }

    override fun showFirstResult(bitmap: Bitmap) {
        image1.setImageBitmap(bitmap)
    }

    override fun showSecondResult(bitmap: Bitmap) {
        image2.setImageBitmap(bitmap)
    }

    override fun showMessage(resId: Int) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT).show()
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}
