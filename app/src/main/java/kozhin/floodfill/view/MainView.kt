package kozhin.floodfill.view

import android.graphics.Bitmap
import android.widget.ImageView

interface MainView {

    fun showProgress()

    fun hideProgress()

    fun showResult(bitmap: Bitmap)

    fun showFirstResult(bitmap: Bitmap)

    fun showSecondResult(bitmap: Bitmap)

    fun showMessage(resId: Int)

    fun showMessage(message: String)

}