package kozhin.floodfill.presenter

import android.graphics.Bitmap
import io.reactivex.Observable
import kozhin.floodfill.model.AlgorithmResult
import kozhin.floodfill.view.MainView

interface MainPresenter {

    fun bindView(view: MainView)

    fun generateBitmap(width: Int, height: Int): Bitmap

    fun selectFirstAlgorithm(position: Int)

    fun selectSecondAlgorithm(position: Int)

    fun setFillSpeed(speed: Int)

    fun startFloodFilling(viewCoords: IntArray,
                            maxImgX: Int,
                            maxImgY: Int,
                            bitmap: Bitmap,
                            absX: Float,
                            absY: Float) : Observable<AlgorithmResult>?

}