package kozhin.floodfill.presenter

import android.graphics.Bitmap
import android.graphics.Color
import io.reactivex.Observable
import kozhin.floodfill.model.*
import kozhin.floodfill.view.MainView
import java.util.*

class MainPresenterImpl: MainPresenter {

    private lateinit var view: MainView
    private var algorithmType1: AlgorithmType = AlgorithmType.NON_RECURSIVE
    private var algorithmType2: AlgorithmType = AlgorithmType.NON_RECURSIVE
    private var fillSpeed = 100
    private var isFill = false
    private lateinit var algorithm: Algorithm

    override fun bindView(view: MainView) {
        this.view = view
    }

    override fun generateBitmap(width: Int, height: Int): Bitmap {
        val random = Random()
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                val color = if (random.nextBoolean()) Color.BLACK else Color.WHITE
                bitmap.setPixel(x, y, color)
            }
        }
        return bitmap
    }

    override fun selectFirstAlgorithm(position: Int) {
        algorithmType1 = AlgorithmType.fromInt(position) ?: AlgorithmType.NON_RECURSIVE
    }

    override fun selectSecondAlgorithm(position: Int) {
        algorithmType2 = AlgorithmType.fromInt(position) ?: AlgorithmType.NON_RECURSIVE
    }

    override fun setFillSpeed(speed: Int) {
        this.fillSpeed = speed
    }

    override fun startFloodFilling(viewCoords: IntArray,
                                     maxImgX: Int,
                                     maxImgY: Int,
                                     bitmap: Bitmap,
                                     absX: Float,
                                     absY: Float) : Observable<AlgorithmResult>? {
        if (isFill) return null
        val imgX = absX - viewCoords[0]
        val imgY = absY - viewCoords[1]
        val maxX = bitmap.width
        val maxY = bitmap.height
        val x = (maxX * imgX / maxImgX.toFloat()).toInt()
        val y = (maxY * imgY / maxImgY.toFloat()).toInt()
        val color = bitmap.getPixel(x, y)
        val isBlack = color == Color.BLACK
        val replacementColor = if (isBlack) Color.WHITE else Color.BLACK
        val observer1 = startAlgorithm(bitmap, Position.FIRST, x, y, color, replacementColor)
        val observer2 = startAlgorithm(bitmap, Position.SECOND, x, y, color, replacementColor)
        return Observable.merge(observer1, observer2)
    }

    private fun startAlgorithm(bitmap: Bitmap, position: Position, x: Int, y: Int, color: Int, replacementColor: Int) : Observable<AlgorithmResult> {
        isFill = true
        val algorithmType = if (position == Position.FIRST) algorithmType1 else algorithmType2
        algorithm = when (algorithmType) {
            AlgorithmType.NON_RECURSIVE -> NonRecursiveAlgorithm(bitmap)
            AlgorithmType.QUEUE_LINEAR -> QueueLinearAlgorithm(bitmap)
            AlgorithmType.RECURSIVE -> RecursiveAlgorithm(bitmap)
        }
        return algorithm.fill(x, y, color, replacementColor)
                .doOnError { isFill = false }
                .flatMap {
                    bitmap.setPixel(it.x, it.y, replacementColor)
                    Observable.just(AlgorithmResult(bitmap, position))
                }
    }

}