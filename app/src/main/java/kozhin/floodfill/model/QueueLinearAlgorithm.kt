package kozhin.floodfill.model

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Point
import io.reactivex.Observable
import java.util.*

class QueueLinearAlgorithm(image: Bitmap) : BaseAlgorithm(image) {

    private var fillColor: Int = -1

    private var startColor = intArrayOf(0, 0, 0)
    private lateinit var pixelsChecked: BooleanArray
    private lateinit var ranges: Queue<FloodFillRange>

     /*fun oldFill(startX: Int, startY: Int, targetColor: Int, replacementColor: Int) {
        this.targetColor = targetColor
        startColor[0] = Color.red(targetColor)
        startColor[1] = Color.green(targetColor)
        startColor[2] = Color.blue(targetColor)
        fillColor = replacementColor

        prepare()

        if (startColor[0] == 0) {
            val startPixel = pixels[width * startY + startX]
            startColor[0] = startPixel shr 16 and 0xff
            startColor[1] = startPixel shr 8 and 0xff
            startColor[2] = startPixel and 0xff
        }

        linearFill(startX, startY)

        var range: FloodFillRange

        while (ranges.size > 0) {
            range = ranges.remove()

            var downPxIdx = width * (range.Y + 1) + range.startX
            var upPxIdx = width * (range.Y - 1) + range.startX
            val upY = range.Y - 1
            val downY = range.Y + 1

            for (i in range.startX..range.endX) {
                if (range.Y > 0 && !pixelsChecked[upPxIdx]
                        && checkPixel(upPxIdx))
                    linearFill(i, upY)

                if (range.Y < height - 1 && !pixelsChecked[downPxIdx]
                        && checkPixel(downPxIdx))
                    linearFill(i, downY)

                downPxIdx++
                upPxIdx++
            }
        }

        image.setPixels(pixels, 0, width, 1, 1, width - 1, height - 1)
    }*/

    override fun fill(startX: Int, startY: Int, targetColor: Int, replacementColor: Int): Observable<Point> {
        return Observable.create {

        }
    }

    fun build(startX: Int, startY: Int, targetColor: Int, replacementColor: Int) : Observable<IntArray> {
        return Observable.create {
            this.targetColor = targetColor
            startColor[0] = Color.red(targetColor)
            startColor[1] = Color.green(targetColor)
            startColor[2] = Color.blue(targetColor)
            fillColor = replacementColor

            prepare()

            if (startColor[0] == 0) {
                val startPixel = pixels[width * startY + startX]
                startColor[0] = startPixel shr 16 and 0xff
                startColor[1] = startPixel shr 8 and 0xff
                startColor[2] = startPixel and 0xff
            }

            linearFill(startX, startY)

            var range: FloodFillRange

            while (ranges.size > 0) {
                it.onNext(pixels)

                range = ranges.remove()

                var downPxIdx = width * (range.Y + 1) + range.startX
                var upPxIdx = width * (range.Y - 1) + range.startX
                val upY = range.Y - 1
                val downY = range.Y + 1

                for (i in range.startX..range.endX) {
                    if (range.Y > 0 && !pixelsChecked[upPxIdx]
                            && checkPixel(upPxIdx))
                        linearFill(i, upY)

                    if (range.Y < height - 1 && !pixelsChecked[downPxIdx]
                            && checkPixel(downPxIdx))
                        linearFill(i, downY)

                    downPxIdx++
                    upPxIdx++
                }
            }
            it.onComplete()
        }
    }

    private fun prepare() {
        pixelsChecked = BooleanArray(pixels.size)
        ranges = LinkedList<FloodFillRange>()
    }

    private fun linearFill(x: Int, y: Int) {
        var lFillLoc = x
        var pxIdx = width * y + x

        while (true) {
            pixels[pxIdx] = fillColor
            pixelsChecked[pxIdx] = true

            lFillLoc--
            pxIdx--

            if (lFillLoc < 0 || pixelsChecked[pxIdx] || !checkPixel(pxIdx)) {
                break
            }
        }

        lFillLoc++

        var rFillLoc = x

        pxIdx = width * y + x

        while (true) {
            pixels[pxIdx] = fillColor
            pixelsChecked[pxIdx] = true

            rFillLoc++
            pxIdx++

            if (rFillLoc >= width || pixelsChecked[pxIdx] || !checkPixel(pxIdx)) {
                break
            }
        }

        rFillLoc--

        val r = FloodFillRange(lFillLoc, rFillLoc, y)

        ranges.offer(r)
    }

    private fun checkPixel(px: Int): Boolean {
        val red = pixels[px].ushr(16) and 0xff
        val green = pixels[px].ushr(8) and 0xff
        val blue = pixels[px] and 0xff

        return (red >= startColor[0] && red <= startColor[0] &&
                green >= startColor[1] && green <= startColor[1] &&
                blue >= startColor[2] && blue <= startColor[2])
    }

}