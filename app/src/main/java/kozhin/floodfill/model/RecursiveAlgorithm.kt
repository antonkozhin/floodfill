package kozhin.floodfill.model

import android.graphics.Bitmap
import android.graphics.Point
import io.reactivex.Observable

class RecursiveAlgorithm(image: Bitmap) : BaseAlgorithm(image) {

    /*fun build(startX: Int, startY: Int, targetColor: Int, replacementColor: Int) {
        val color = image.getPixel(startX, startY)
        if (color != targetColor) return

        image.setPixel(startX, startY, replacementColor)

        val up = maxMin(startY + 1, 0, image.height - 1)
        val down = maxMin(startY - 1, 0, image.height - 1)
        val left = maxMin(startX - 1, 0, image.width - 1)
        val right = maxMin(startX + 1, 0, image.width - 1)

        fill(startX, up, targetColor, replacementColor)
        fill(startX, down, targetColor, replacementColor)
        fill(left, startY, targetColor, replacementColor)
        fill(right, startY, targetColor, replacementColor)
    }*/

    override fun fill(startX: Int, startY: Int, targetColor: Int, replacementColor: Int) : Observable<Point> {
        return step(startX, startY, targetColor)
                .flatMap {
                    val up = maxMin(startY + 1, 0, height - 1)
                    val down = maxMin(startY - 1, 0, height - 1)
                    val left = maxMin(startX - 1, 0, width - 1)
                    val right = maxMin(startX + 1, 0, width - 1)
                    Observable.mergeArray(
                            Observable.just(it),
                            fill(startX, up, targetColor, replacementColor),
                            fill(startX, down, targetColor, replacementColor),
                            fill(left, startY, targetColor, replacementColor),
                            fill(right, startY, targetColor, replacementColor))
                }
    }

    private fun step(x: Int, y: Int, targetColor: Int) : Observable<Point> {
        return Observable.create {
            val color = getPixel(x, y)
            if (color != targetColor) it.onComplete()
            it.onNext(Point(x, y))
        }
    }

    private fun maxMin(value: Int, min: Int, max: Int): Int = Math.max(min, Math.min(value, max))

}