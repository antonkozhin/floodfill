package kozhin.floodfill.model

import android.graphics.Bitmap

abstract class BaseAlgorithm(image: Bitmap) : Algorithm {

    protected var width = 0
    protected var height = 0
    protected var pixels: IntArray
    protected var targetColor: Int = 0

    init {
        width = image.width
        height = image.height
        pixels = IntArray(width * height)
        image.getPixels(pixels, 0, width, 1, 1, width - 1, height - 1)
    }

    fun getPixel(x: Int, y: Int) : Int {
        return pixels[width * y + x]
    }

}